'use strict';
const backgrounds = [...(document.getElementsByClassName('theme-background-color'))];
const fonts = [...(document.getElementsByClassName('theme-font-color'))];
const color1 = 'yellowgreen';
const color2 = 'darkgrey';
changePageTheme(backgrounds, fonts, color1, color2);

function changePageTheme(backgroundElements, fontElements, basicColor, optionalColor) {
    const themeButton = document.getElementsByClassName('theme-button')[0];
    window.onload = function () {
        if (localStorage.getItem('themeColor') !== null) {
            let color = localStorage.getItem('themeColor');
            backgroundElements.forEach((element) => {
                element.style.backgroundColor = color;
            });
            fontElements.forEach((element) => {
                element.style.color = color;
            });
            themeButton.style.backgroundColor = color;
        } else {
            themeButton.style.backgroundColor = basicColor;

            backgroundElements.forEach((element) => {
                element.style.backgroundColor = basicColor;
            });
            fontElements.forEach((element) => {
                element.style.color = basicColor;
            });
        }

    };


    themeButton.addEventListener('click', () => {
        if (themeButton.style.backgroundColor !== optionalColor) {
            backgroundElements.forEach((element) => {
                element.style.backgroundColor = optionalColor;
            });
            fontElements.forEach((element) => {
                element.style.color = optionalColor;
            });
            themeButton.style.backgroundColor = optionalColor;
            localStorage.setItem('themeColor', optionalColor);

        } else {
            backgroundElements.forEach((element) => {
                element.style.backgroundColor = basicColor
            });
            fontElements.forEach((element) => {
                element.style.color = basicColor
            });
            themeButton.style.backgroundColor = basicColor;
            localStorage.setItem('themeColor', basicColor)
        }

    });
}