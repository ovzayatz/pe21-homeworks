'use strict';

function createNewUser() {
    let firstName = prompt('Please enter your first name');
    while (!isNaN(+firstName)) {
        firstName = prompt('Please enter correct first name')
    }
    let lastName = prompt('Please enter your last name');
    while (!isNaN(+lastName)) {
        lastName = prompt('Please enter correct last name')
    }

    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function () {
            return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase())
        }
    };
    return newUser;
}

const user1 = createNewUser();
user1.getLogin();
console.log(user1);
console.log(user1.getLogin());