$('.anchor-links-item a').on('click', function (e) {
    e.preventDefault();
    const href = $(this).attr('href');
    const offset = $(href).offset().top;
    $('html, body').animate({
        scrollTop: offset,
    }, 800);
});


$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > innerHeight) {
            $('.up-button').fadeIn();
        } else {
            $('.up-button').fadeOut();
        }
    });

    $('.up-button').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0,
        }, 800);
    });

});

$('.toggle-posts').click(function () {
    $('.popular_posts_section').slideToggle("slow");
});