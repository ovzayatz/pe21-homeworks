'use strict';

const buttonsCollection = document.getElementsByClassName('btn');


for (let i=0; i<buttonsCollection.length; i++) {
    document.addEventListener('keydown', (event) => {

        if (buttonsCollection[i].dataset.buttonCode.toLowerCase() === event.code.toLowerCase()) {
            buttonsCollection[i].classList.add('background-blue');
        }
        else {
            buttonsCollection[i].classList.remove('background-blue');
        }
    })
}