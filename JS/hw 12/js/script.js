'use strict';

const imagesArray = [...document.getElementsByClassName('image-to-show')];
const playButton = document.getElementById('play-button');
const stopButton = document.getElementById('stop-button');

function showImage(imgArr, interval, index) {

    let timerID = setInterval(() => {
        imgArr.forEach((e) => {
            e.classList.add('hidden');
        });

        if (index > (imgArr.length - 1)) {
            index = 0
        }
        imgArr[index].classList.remove('hidden');
        stopButton.addEventListener('click', () => {
            clearInterval(timerID);
            stopButton.classList.add('hidden');
            playButton.classList.remove('hidden')
        });
        playButton.addEventListener('click', () => {
            showImage(imagesArray, 10000, index);
            stopButton.classList.remove('hidden');
            playButton.classList.add('hidden')
        });
        index++;

    }, interval);
}

showImage(imagesArray, 10000, 1);

