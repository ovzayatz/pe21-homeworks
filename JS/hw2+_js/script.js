'use strict';

function isPrime(num) {
    if (num <= 1) return false;
    if (num % 2 === 0 && num > 2) return false;
    let s = Math.sqrt(num);
    for(let i = 3; i <= s; i++) {
        if(num % i === 0) return false;
    }
    return true;
}

let smallerPrime = prompt('Please enter first prime number', '2');
while (!isPrime(smallerPrime)) {
    smallerPrime = prompt('Please enter first really prime number.', '2');
}

let biggerPrime = prompt('Please enter second prime number', '11');
while (!isPrime(biggerPrime)) {
    biggerPrime = prompt('Please enter second really prime number.', '11');
}
    for (let i=+smallerPrime; i<=+biggerPrime; i++) {
        if (isPrime(i)) {
            console.log(i)
        }
    }
