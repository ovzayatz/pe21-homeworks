'use strict';

function createStudentProfile() {
    const student = {};
    let studentName = prompt('Введите имя студента', 'Имя');
    while (!isNaN(+studentName)) {
        studentName = prompt('Введите правильное имя студента', 'Имя')
    }
    let studentLastName = prompt('Введите фамилию студента', 'Фамилия');
    while (!isNaN(+studentLastName)) {
        studentLastName = prompt('Введите правильную фамилию студента', 'Фамилия')
    }
    student.name = studentName;
    student['last name'] = studentLastName;
    student.table = {};
    while (true) {
        let subject = prompt("Введите название предмета");
        while (!isNaN(+subject)) {
            if (!subject) {
                break;
            }
            subject = prompt("Введите правильное название предмета");

        }
        if (!subject) {
            break;
        }
        let grade = prompt("Введите оценку по предмету");
        while (isNaN(+grade) || +grade < 0 || +grade > 12) {
            grade = prompt("Введите правильную оценку");
        }

        student.table[subject] = grade;
    }
    return student;
}

const student = createStudentProfile();

function studentEvaluation(studentProfile) {
    let lowGrades = 0;
    let gradesSum = 0;
    let courseQuantity = 0;

    for (let key in studentProfile.table) {
        if (+(studentProfile.table[key]) < 4) {
            lowGrades += 1
        }
    }
    if (lowGrades < 1) {
        alert('Студент переведен на следующий курс');

        for (let key in studentProfile.table) {
            courseQuantity++;
            gradesSum += studentProfile.table[key];
        }

        const averageGrade = gradesSum / courseQuantity;

        if (+averageGrade > 7) {
            alert("Студенту назначена стипендия");
        }
    } else {
        alert('Студент не переведен на следующий курс. Есть оценки ниже 4');
    }
}

studentEvaluation(student);