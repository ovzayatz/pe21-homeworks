'use strict';


function passwordVisibilityChanging (input, icon) {

         icon.addEventListener('click', ()=>{
                 if (icon.classList[1] === 'fa-eye') {
                     input.setAttribute('type', 'text');
        icon.setAttribute('class', 'fas fa-eye-slash icon-password')
} else if (icon.classList[1] === 'fa-eye-slash'){
                     input.setAttribute('type', 'password');
                     icon.setAttribute('class', 'fas fa-eye icon-password')
   }})}



   function comparePasswords (pass, passConfirm, confirmBtn, errorMessageElement) {

confirmBtn.addEventListener('click', (event)=> {

    if (pass.value===''||passConfirm.value==='') {
            alert('Please enter both password and password confirmation values')
    } else if (pass.value !== passConfirm.value) {
        event.preventDefault();
        errorMessageElement.innerText = 'Please enter similar passwords'

    } else {
        errorMessageElement.remove();
        alert('You are welcome');
    }
})

   }



const passwordInput = document.getElementById('password-input');
const passwordConfirmationInput = document.getElementById('password-confirmation-input');
const passwordIcon = document.getElementById('pass-i');
const passwordConfirmationIcon = document.getElementById('pass-conf-i');
const confirmationButton = document.getElementById('btn');
const errorMessage = document.createElement('span');
errorMessage.classList.add('error-message');
confirmationButton.before(errorMessage);
passwordVisibilityChanging(passwordInput, passwordIcon);
passwordVisibilityChanging(passwordConfirmationInput, passwordConfirmationIcon);
comparePasswords(passwordInput, passwordConfirmationInput, confirmationButton, errorMessage);
