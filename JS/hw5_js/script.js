'use strict';


function createNewUser() {
    const newUser = {};

    let firstName = prompt('Please enter your first name');
    while (!isNaN(+firstName)) {
        firstName = prompt('Please enter correct first name')
    }
    let lastName = prompt('Please enter your last name');
    while (!isNaN(+lastName)) {
        lastName = prompt('Please enter correct last name')
    }

    newUser.birthday = prompt('Please enter your date of birth in dd.mm.yyyy', 'dd.mm.yyyy');

// dateValidation(newUser.birthday);
//
//     function dateValidation (userDate) {
//         let userDateMonthDayYear = new Date(userDate.split('.') [1] + '.' + userDate.split('.') [0] + '.' + userDate.split('.') [2]);
//         let userDateTimestamp = userDateMonthDayYear.getTime();
//         while (isNaN(userDateTimestamp) || new Date(userDateMonthDayYear) < new Date(1920, 1, 1)) {
//             userDate = prompt('Please enter correct date of birth in dd.mm.yyyy', 'dd.mm.yyyy');
//         }
//                 return userDate;
//     }

    Object.defineProperty(newUser, 'firstName', {
            value: firstName,
            writable: false,
            enumerable: true,
            configurable: true
        }
    );
    Object.defineProperty(newUser, 'lastName', {
            value: lastName,
            writable: false,
            enumerable: true,
            configurable: true
        }
    );


    newUser.setFirstName = function (name) {
        if (isNaN(+name)) {
            Object.defineProperty(newUser, 'firstName', {
                    value: name
                }
            );
            return true;
        }
        return false;
    };
    newUser.setLastName = function (surname) {
        if (isNaN(+surname)) {
            Object.defineProperty(newUser, 'lastName', {
                    value: surname
                }
            );
            return true;
        }
        return false;
    };

    newUser.getLogin = function () {
        return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase())
    };
    newUser.getLogin();

    newUser.getAge = function () {
        let userDateMonthDayYear = new Date(this.birthday.split('.') [1] + '.' + this.birthday.split('.') [0] + '.' + this.birthday.split('.') [2]);
        let userDateTimestamp = userDateMonthDayYear.getTime();
        newUser.age = Math.floor((new Date().getTime() - userDateTimestamp) / (1000 * 60 * 60 * 24 * 365));
        return newUser.age;
    };

    newUser.getAge();

    newUser.getPassword = function () {
        newUser.password =  this.firstName.charAt(0).toUpperCase()+this.lastName.toLowerCase()+ this.birthday.split('.')[2];
        return newUser.password;
    };
    newUser.getPassword();

    console.log('user login -', newUser.getLogin());
    console.log('user password -', newUser.password);
    console.log('user age -', newUser.age);
    return newUser;
}
let newUser = createNewUser();
console.log(newUser);

