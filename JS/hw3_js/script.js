'use strict';


function calculator (number1, number2, operation) {
    number1 = prompt('Please enter first number', '1');
    while (isNaN(+number1)||number1==null||number1==='') {
        number1 = prompt('Please enter correct first number', '1');
    }

    number2 = prompt('Please enter second number', '2');
    while (isNaN(+number2)||number2==null||number2==='') {
        number2 = prompt('Please enter correct second number', '2');
    }

    operation = prompt('Please choose operation', '+');
    while (operation!=='+' && operation!=='-' && operation!=="*" && operation!=='/') {
        operation = prompt('Please choose correct operation', '+');
    }

    switch (operation) {

        case '+':
            alert('The sum of your numbers is: ' + ((+number1) + (+number2)));
            break;
        case '-':
            alert('Subtraction of your numbers is: ' + (number1 - number2));
            break;
        case '*':
            alert('Multiplication of your numbers is: ' + (number1 * number2));
            break;
        case '/':
            alert('Division of your numbers is: ' + (number1 / number2));
            break;
    }

}

calculator ();