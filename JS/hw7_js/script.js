'use strict';


function getHtmlListFromArray(array) {
    const htmlList = document.createElement('ul');
    let listItemsArray = [];
    array.map((arrayElement) => { listItemsArray.push(`<li> ${arrayElement} </li>`)});
        htmlList.innerHTML = listItemsArray.toString().replace(/,/g, '');
        document.body.append(htmlList);
}



function clearBodyWithTimer(seconds) {

    const timerBlock = document.createElement('div');
    timerBlock.classList.add('timer');
    document.body.append(timerBlock);

    let current = seconds;
    let timerId = setInterval(() => {
        timerBlock.textContent = `${current}`;
        if (current === 0) {
            clearInterval(timerId);
            document.body.innerHTML = "";
        }
        current--;
    }, 1000);
}


getHtmlListFromArray(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
clearBodyWithTimer(10);

