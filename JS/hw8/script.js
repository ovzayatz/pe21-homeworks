'use strict';
const priceInput = document.createElement('input');
priceInput.classList.add('priceField');
priceInput.id = 'priceFieldId';
priceInput.placeholder = '0.00';


priceInput.type = 'number';
const priceInputLabel = document.createElement('label');
priceInputLabel.innerText = 'Price';
priceInputLabel.classList.add('priceFieldLabel');
priceInputLabel.htmlFor = 'priceFieldId';

priceInput.addEventListener('focus', () => {
    priceInput.style.border = '2px solid limegreen'
});
priceInput.addEventListener('focusout', () => {
    priceInput.style.border = '';
    priceAnalysis(priceInput.value);
});

const currentPriceSpan = document.createElement('span');
currentPriceSpan.classList.add('currentPrice');
const exitCurrentPriceButton = document.createElement('span');
exitCurrentPriceButton.classList.add('exitCurPrBut');

const errorMessage = document.createElement('p');
errorMessage.classList.add('errorMessageBlock');
errorMessage.innerText = 'Please enter correct price';

function priceAnalysis(price) {
    if (+price === 0) {
        priceInput.value = '';

    } else if (price < 0) {

        priceInput.style.border = '2px solid red';
        document.body.append(errorMessage);
        priceInput.style.cssText = 'color:""';
        currentPriceSpan.remove();
        exitCurrentPriceButton.remove()

    } else if (price > 0) {
        currentPriceSpan.innerText = `Current price: ${price} hryvnas`;
        exitCurrentPriceButton.innerText = 'x';
        errorMessage.remove();
        document.body.prepend(currentPriceSpan);
        document.body.prepend(exitCurrentPriceButton);
        priceInput.style.cssText = 'color: limegreen';
        exitCurrentPriceButton.addEventListener('click', () => {
            currentPriceSpan.remove();
            exitCurrentPriceButton.remove();
            priceInput.value = '';
        })
    }


}

document.body.append(priceInputLabel);
document.body.append(priceInput);
