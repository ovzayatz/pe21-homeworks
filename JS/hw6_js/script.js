'use strict';

function fillerBy(array, dataType) {
    const resultArray = [];

    array.forEach(function cb(item) {
        if (typeof item !== dataType) {//------------------> проверка на соответвие заданному в функции аргументу типа данных
            if (!(dataType === 'null' && item === null)) { //-------> исключение null если он задан в аргументе функции
                return resultArray.push(item);
            }
        } else if (typeof item === 'object' && +item === 0) {
            return resultArray.push(item);
        }
    });
    return resultArray;
}

let userArray = ['Dart', 12, true, null, undefined, {name: 'Dart'}, 99999999999999999999n, Symbol()];

console.log(fillerBy(userArray, 'string'));