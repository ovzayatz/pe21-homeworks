'use strict';


const tabs = document.getElementsByClassName('tabs-title');
const texts = document.getElementsByClassName('hero-text');

function showTextOnTabClick(tabElementsCollection, textElementsCollection) {

    for (let t = 0; t < textElementsCollection.length; t++) {
        textElementsCollection[t].style.cssText = 'display: none'
    }

    for (let i = 0; i < tabElementsCollection.length; i++) {
        tabElementsCollection[i].addEventListener('click', () => {
            for (let j = 0; j < textElementsCollection.length; j++) {
                if (textElementsCollection[j].dataset.heroName === tabElementsCollection[i].dataset.heroName) {
                    for (let t = 0; t < textElementsCollection.length; t++) {
                        textElementsCollection[t].style.cssText = 'display: none'
                    }
                    textElementsCollection[j].style.cssText = 'display: block';
                    for (let r = 0; r < tabElementsCollection.length; r++) {
                        tabElementsCollection[r].classList.remove('active')
                    }
                    tabElementsCollection[i].classList.add('active');

                }
            }

        })
    }
}

showTextOnTabClick(tabs, texts);