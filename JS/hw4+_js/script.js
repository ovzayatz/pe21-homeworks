'use strict';

function createNewUser() {

    let firstName = prompt('Please enter your first name');
    while (!isNaN(+firstName)) {
        firstName = prompt('Please enter correct first name')
    }
    let lastName = prompt('Please enter your last name');
    while (!isNaN(+lastName)) {
        lastName = prompt('Please enter correct last name')
    }

    const newUser = {};

    Object.defineProperty(newUser, 'firstName', {
            value: firstName,
            writable: false,
            enumerable: true,
            configurable: true
        }
    );
    Object.defineProperty(newUser, 'lastName', {
            value: lastName,
            writable: false,
            enumerable: true,
            configurable: true
        }
    );
    newUser.getLogin = function () {
        return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase())
    };

    newUser.setFirstName = function (name) {
        if (isNaN(+name)) {
            Object.defineProperty(newUser, 'firstName', {
                    value: name
                }
            );
            return true;
        }
        return false;
    };
    newUser.setLastName = function (surname) {
        if (isNaN(+surname)) {
            Object.defineProperty(newUser, 'lastName', {
                    value: surname
                }
            );
            return true;
        }
        return false;
    };
//-----------------------------------------------------------------------------------------
//Для изменения имени и фамилии - вызвать функции:
// newUser.setFirstName('Luke')
// newUser.setLastName('Skywalker')
// ----------------------------------------------------------------------------------------

    console.log(newUser.getLogin());
    return newUser;
}

let user1 = createNewUser();
console.log(user1);

