const burger = document.getElementsByClassName('burger')[0];
const menu = document.getElementsByClassName('header__menu')[0];
const menuItemsArray = [... document.getElementsByClassName ('menu__item')];

burger.addEventListener('click',  () => {
    burger.classList.toggle('burger-toggle');
    menu.classList.toggle('header__menu--mb-show');
});

menuItemsArray.forEach((element)=>{
    element.addEventListener('click', (e) => {

        menuItemsArray.forEach((el)=>{
            el.children[0].classList.remove('menu__link--active')
        });

        e.target.classList.add('menu__link--active');
        e.target.children[0].classList.add('menu__link--active');

        menuItemsArray.forEach((el)=>{
            el.classList.remove('menu__link--active')
        });
    });
});

