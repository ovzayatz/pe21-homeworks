module.exports = [
    {
        "name": "tent",
        "price": 200,
        "url": "https://terraincognita.com.ua/image/cache/data/products/Terra%20Incognita/Baltora%204/baltora_4[532]-150x150.jpg",
        "art": 1
    },
    {
        "name": "backpack",
        "price": 150,
        "url": "https://terraincognita.com.ua/image/cache/data/products/Terra%20Incognita/Backpack/Discover/38721thickbox-150x150.jpg",
        "art": 2,
        "color": "black"
    },
    {
        "name": "sleeping bag",
        "price": 80,
        "url": "https://terraincognita.com.ua/image/cache/data/products/Terra%20Incognita/Sleep/Siesta/38722thickbox-150x150.jpg",
        "art": 3,
        "color": "lime"
    },
    {
        "name": "mat",
        "price": 50,
        "url": "https://terraincognita.com.ua/image/cache/data/products/Thermarest/Ridge%20Rest%20Classic/Ridge%20Rest%20Classic%20Regular-2-150x150.jpg",
        "art": 4
    },
    {
        "name": "lamp",
        "price": 10,
        "url": "https://terraincognita.com.ua/image/cache/data/products/AceCamp/Glow%20Flashlight/6d1b59dae3fcb71a92cdfac88cb0e2ae-150x150.jpg",
        "art": 5,
        "color": "white"
    },
    {
        "name": "mug",
        "price": 5,
        "url": "https://terraincognita.com.ua/image/cache/data/products/Terra%20Incognita/Mug/s_mug[478]-150x150.jpg",
        "art": 6,
        "color": "metal"
    },
    {
        "name": "knife",
        "price": 20,
        "url": "https://terraincognita.com.ua/image/cache/data/products/Gerber%20Bear/Compact%20Scout/1392730728_6-150x150.jpg",
        "art": 7,
        "color": "orange-black"
    },
    {
        "name": "compass",
        "price": 20,
        "url": "https://terraincognita.com.ua/image/cache/data/products/AceCamp/AceCamp%20Classic%20Map%20Compass/acecamp_0003110_images_11888501148-150x150.jpg",
        "art": 8,
        "color": "orange"
    },
    {
        "name": "glasses",
        "price": 30,
        "url": "https://terraincognita.com.ua/image/cache/data/products/asics/Asics/Galaxy/GALAXY%20GREY(2)-520x460-500x500-150x150.JPG",
        "art": 9
    },
    {
        "name": "gas",
        "price": 10,
        "url": "https://terraincognita.com.ua/image/cache/data/products/Primus/Gas/Power%20Gas%20100%20%D0%B3/Power%20Gas%20100%20%D0%B3-2-150x150.png",
        "art": 10,
        "color": "red"
    }
];