const http = require('http');
const fs = require('fs');
const url = require('url');

const port = process.env.PORT || 8085;


const requestHandler = (req, resp) => {

    if (req.method === 'GET') {
        if (req.url === '/api/goodsList') {
            fs.readFile('./public/goodsList.json', (err, data) => {
                err && console.error(err);
                resp.end(data.toString())
            });
        }
    }
};

const server = () => {
    const myServer = http.createServer(requestHandler);
    myServer.listen(port, err => {
        err && console.error(err);
        console.log(`server listening on port ${port}`);
    });
};

server();

