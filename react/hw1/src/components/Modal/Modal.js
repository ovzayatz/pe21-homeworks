import React from 'react';

function Modal(props) {

    const {header, text, className, onClick, closeButton, actions: {okButton, cancelButton, closeButtonElement}} = props;
    return (
        <div className={className} onClick={onClick}>
            <div className='modal__window'>
                <div className='modal__header'>
                    <span className='modal__header-text'>{header}</span>
                    {closeButton ? closeButtonElement : undefined}
                </div>
                <p>{text}</p>
                {okButton}
                {cancelButton}
            </div>
        </div>

    )
}

export default Modal;