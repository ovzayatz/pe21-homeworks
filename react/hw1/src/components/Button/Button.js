import React, {PureComponent} from 'react';

class Button extends PureComponent {


    render() {
        const {className, backgroundColor, text, onClick} = this.props;
        return (
            <button
                className={className}
                style={{backgroundColor: backgroundColor}}
                onClick={onClick}
            >{text}</button>
        );
    }
}

export default Button;