import React, {PureComponent} from 'react';
import './App.scss';
// import Button from './components/Button/Button'
// import Modal from "./components/Modal/Modal";
// import './components/Button/Button.scss'
// import './components/Modal/Modal.scss'
import GoodsItem from "../../hw2/src/components/GoodsItem/GoodsItem";


class App extends PureComponent {
    state = {

    };

    componentDidMount() {
        this.getGoods();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.getGoods();
    }


    getGoods() {
        fetch('./goodsList.json').then(r => r.json())
            .then(data => this.setState({goods: data.goods}));
    };

    render() {
        return (
            <div className="App">
                {this.state.goods.forEach((el)=> {
                    return <GoodsItem className = 'goodsItem' name = {el.name} price = {el.price} url = {el.url} art = {el.art} color ={el.color}/>
                })}
            </div>

        );

    }
}


export default App;
