import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'


const Button = ({className, backgroundColor, text, onClick}) => {
    return (
        <button
            className={className}
            style={{backgroundColor: backgroundColor}}
            onClick={onClick}
            data-testid="button"
        >{text}</button>
    );
};


Button.propTypes = {
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

Button.defaultProps = {
    backgroundColor: 'lightgrey',
    className: 'btn'
};

export default Button;