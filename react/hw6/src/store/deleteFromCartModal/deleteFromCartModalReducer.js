import initialStore from "../initialStore";
import {TOGGLE_DELETE_FROM_CART_MODAL} from "./deleteFromCartModalActions";

export default function delFromCartModalReducer(isDelCartModalOpenStore = initialStore.isDelCartModalOpen, {type, payload}) {
    switch (type) {

        case TOGGLE_DELETE_FROM_CART_MODAL:
            return payload;

        default:
            return isDelCartModalOpenStore
    }
}