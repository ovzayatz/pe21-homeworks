import initialStore from "../initialStore";
import {TOGGLE_ADD_TO_CART_MODAL} from "./addToCartModalActions";

export default function addToCartModalReducer(isCartModalOpenStore = initialStore.isCartModalOpen, {type, payload}) {
    switch (type) {

        case TOGGLE_ADD_TO_CART_MODAL:
            return payload!==undefined ? payload : isCartModalOpenStore;

        default:
            return isCartModalOpenStore
    }
}