import {combineReducers} from "redux";
import goodsReducer from "../goods/goodsReducer";
import addToCartModalReducer from "../addToCartModal/addToCartModalReducer";
import delFromCartModalReducer from "../deleteFromCartModal/deleteFromCartModalReducer";
import cartFormReducer from "../cartForm/cartFormReducer";


export default combineReducers({
   goods: goodsReducer,
   isCartModalOpen: addToCartModalReducer,
   isDelCartModalOpen: delFromCartModalReducer,
   formValues: cartFormReducer
})