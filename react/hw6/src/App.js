import React, {useEffect, useState} from 'react';
import './App.scss';
import AppRoutes from "./routes/AppRoutes";
import {NavLink} from "react-router-dom";
import {loadingGoodsAction} from "./store/goods/goodsActions";
import {connect} from "react-redux";

const App = ({loadGoods}) => {

    const [favourites, setFavourites] = useState(JSON.parse(localStorage.getItem('favourites')) || []);
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);

    useEffect(() => {
        loadGoods();
    }, [loadGoods]);


    const getLocalStorageItems = (key) => {
        return JSON.parse(localStorage.getItem(key))
    };

    const changeState = () => {
        setFavourites(getLocalStorageItems('favourites') || []);
        setCart(getLocalStorageItems('cart') || []);
    };

    return (

        <div className="App">
            <div className='header'>
                <a href='/' className='logo'><span className='logo__text-p1'>hiking</span> <span
                    className='logo__text-p2'>shop</span> </a>
                <div>
                    <NavLink to={'/favourites'} className='header__link' activeStyle={{color:'#E74C3C'}}>Favourites</NavLink>
                    <NavLink to={'/cart'} className='header__link' activeStyle={{color:'#E74C3C'}}>Cart</NavLink>
                </div>
            </div>
            <AppRoutes
                favourites={favourites}
                cartArr={cart}
                setCart={setCart}
                changeState={() => changeState()}
                getLocalStorageItems={() => getLocalStorageItems()}
            />
        </div>
    );
};

const mapStoreToProps = (store) => {
    return {goods: store.goods}
};

const mapDispatchToProps = (dispatch) => ({
    loadGoods: () => dispatch(loadingGoodsAction())
});

export default connect(mapStoreToProps, mapDispatchToProps)(App);


