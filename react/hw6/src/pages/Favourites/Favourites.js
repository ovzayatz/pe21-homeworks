import React from "react";
import GoodsItem from "../../components/GoodsItem/GoodsItem";
import PropTypes from 'prop-types';


const Favourites = ({changeState, getLocalStorageItems}) => {
    const favouritesArr = JSON.parse(localStorage.getItem('favourites')) || [];

    const favouritesSection = favouritesArr &&
        <div className='section'>
            <h2 className='section-heading'>Please checkout your favourite goods</h2>
            <div className='goods-list'>
                {favouritesArr.map(el => {
                    return <GoodsItem price={el.price} name={el.name} art={el.art} color={el.color} url={el.url}
                                      className={el.className} key={el.art} changeState={changeState}
                                      getLocalStorageItems={getLocalStorageItems} cartBtn={true}/>
                })}
            </div>
        </div>;

    if (favouritesArr.length > 0) {
        return (favouritesSection)
    } else {
        return <h2 className='section-heading'>You don't have favourites yet</h2>;
    }

};

Favourites.propTypes = {
    changeState: PropTypes.func.isRequired,
    getLocalStorageItems: PropTypes.func.isRequired
};

export default Favourites;


