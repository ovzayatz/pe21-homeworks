import React, {PureComponent} from 'react';
import './App.scss';
import GoodsItem from "./components/GoodsItem/GoodsItem";

class App extends PureComponent {
    state = {
        goods: [],
    };

    componentDidMount() {
        this.getGoods();
    }

    getGoods() {
        fetch('./goodsList.json').then(r => r.json())
            .then(data => this.setState({goods: data.goods}));
    };

    render() {
        return (
            <div className="App">
                <div className='header'>
                    <a href='#' className='logo'><span className='logo__text-p1'>hiking</span> <span className='logo__text-p2'>shop</span> </a>
                </div>
                <div className='goods-list'>
                {this.state.goods.map((el) => {
                    return <GoodsItem className={'goods-item'} name={el.name} price={el.price} url={el.url} art={el.art}
                                      color={el.color} key={el.art}
                    />
                })}
                </div>
            </div>

        );

    }
}


export default App;
