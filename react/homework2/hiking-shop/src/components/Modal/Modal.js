import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import './Modal.scss'


class Modal extends PureComponent {
    render() {
        const {header, text, className, onClick, imageURL, art, switchModal, itemName} = this.props;
        const addToCart = (id) => {
            if (localStorage.getItem('cart')) {
                let cart = JSON.parse(localStorage.getItem('cart'));
                cart.push(id);
                localStorage.setItem('cart', JSON.stringify(cart))
            } else {
                localStorage.setItem('cart', JSON.stringify([id]))
            }
        };

        return (
            <div className={className} onClick={onClick}>
                <div className='cart-modal__window'>
                    <div className='modal__header'>
                        <p className='modal__header-text'>{header}</p>
                    </div>
                    <img alt='item photo' src={imageURL}/>
                    <p>{text}</p>
                    <Button className='cart-modal__btn btn' text='Add to cart' onClick={() => {
                        addToCart(art);
                        alert(`${itemName} is added to your cart`);
                        switchModal()}}/>
                    <Button className='cart-modal__btn btn' text='Cancel' onClick={() => {
                        switchModal()
                    }}/>

                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    imageURL: PropTypes.string,
    art: PropTypes.number,
    switchModal: PropTypes.func,
    itemName: PropTypes.string
};

export default Modal;