import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import './Button.scss'


class Button extends PureComponent {


    render() {
        const {className, backgroundColor, text, onClick} = this.props;
        return (
            <button
                className={className}
                style={{backgroundColor: backgroundColor}}
                onClick={onClick}
            >{text}</button>
        );
    }
}

Button.propTypes = {
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

Button.defaultProps = {
 backgroundColor: "lightgrey"
};

export default Button;