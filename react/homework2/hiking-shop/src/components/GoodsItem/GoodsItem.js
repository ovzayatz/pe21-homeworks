import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import './GoodsItem.scss'
import Modal from "../Modal/Modal";
import Button from "../Button/Button";

class GoodsItem extends PureComponent {

    state = {
        isCartModalOpen: false,
        isFavorite: false
    };


    switchState = (stateKey) => this.setState({[stateKey]: !this.state[stateKey]});

    addToFavourites = (id) => {
        if (localStorage.getItem('favourites')) {
            let favourites = JSON.parse(localStorage.getItem('favourites'));
            if (!favourites.includes(id)) {
                favourites.push(id);
                localStorage.setItem('favourites', JSON.stringify(favourites))
            }
        } else {
            localStorage.setItem('favourites', JSON.stringify([id]))
        }
    };

    deleteFromFavourites = (id) => {
        let favourites = JSON.parse(localStorage.getItem('favourites'));
        const favToDelete = favourites.find(e => e === id);
        favourites.splice(favourites.indexOf(favToDelete), 1);
        localStorage.setItem('favourites', JSON.stringify(favourites))
    };


    render() {


        const {name, price, url, art, color, className} = this.props;

        if (JSON.parse(localStorage.getItem('favourites'))) {
            if (JSON.parse(localStorage.getItem('favourites')).find(e => e === art)) {
                this.setState({isFavorite: true});
            }
        } else {
            localStorage.setItem('favourites', JSON.stringify([]))
        }


        const cartModal = this.state.isCartModalOpen && <Modal
            itemName={name}
            header={`add ${name} to cart`}
            text={`Please push Ok to add ${name} to cart`}
            className='cart-modal'
            imageURL={url}
            art={art}
            onClick={(event) => {
                if (event.target === event.currentTarget) this.switchState('isCartModalOpen')
            }}
            switchModal={() => this.switchState('isCartModalOpen')}
        />;



        return (
            this.state.isCartModalOpen ? cartModal :
                <div className={className}>
                    <div><img alt='item photo' src={url}/></div>
                    <div className={'goods-item__name-wrapper'}>
                        <span>{name}</span>
                        <img className='goods-item__fav-icon'
                             src={!this.state.isFavorite ? './icons/star_grey.png' : './icons/star_gold.png'}
                             alt='favourites icon' onClick={() => {
                            if (!this.state.isFavorite) {
                                this.addToFavourites(art);
                                this.switchState('isFavorite')
                            } else {
                                this.switchState('isFavorite');
                                this.deleteFromFavourites(art)
                            }
                        }}/>
                    </div>
                    <p>{`Price: ${price}`}</p>
                    <p>{`ID: ${art}`}</p>
                    <p>{`Color: ${color}`}</p>
                    <Button className='goods-item_cart-button btn' text='Add to cart'
                            onClick={() => this.switchState('isCartModalOpen')}/>
                </div>
        );
    }
}

GoodsItem.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    url: PropTypes.string,
    art: PropTypes.number.isRequired,
    color: PropTypes.string,
    className: PropTypes.string
};

GoodsItem.defaultProps = {
    color: 'in stock'
};

export default GoodsItem;

