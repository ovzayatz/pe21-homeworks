import React, {useState} from 'react';
import GoodsItem from "../../components/GoodsItem/GoodsItem";


const Main = ({goods, changeState}) => {


    return (

        <div className='goods-list'>
            {goods.map((el) => {
                return <GoodsItem className={'goods-item'} name={el.name} price={el.price} url={el.url} art={el.art}
                                  color={el.color} key={el.art} cartBtn={true} changeState={changeState}
                />
            })}
        </div>
    );
};

export default Main;