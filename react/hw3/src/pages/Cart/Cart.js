import React from "react";
import GoodsItem from "../../components/GoodsItem/GoodsItem";
import PropTypes from 'prop-types';

const Cart = ({cartArr, changeState}) => {
    cartArr = JSON.parse(localStorage.getItem('cart'))||[];

    return (
        <div className='favourites'>
            <h2 className='favourites__heading'>Please checkout your cart</h2>
            <div className='favourites__container'>
                {cartArr.map(el => {
                    return <GoodsItem price={el.price} name={el.name} art={el.art} color={el.color} url={el.url}
                                      className={el.className} key={el.art} cartBtn={false} delCartBtn={true}
                                      changeState={changeState}
                    />
                })}
            </div>
        </div>
    );
};

Cart.propTypes = {
    cartArr: PropTypes.array.isRequired,
    changeState: PropTypes.func.isRequired
};


export default Cart;