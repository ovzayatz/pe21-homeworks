import React from "react";
import GoodsItem from "../../components/GoodsItem/GoodsItem";
import PropTypes from 'prop-types';


const Favourites = ({changeState, getLocalStorageItems}) => {
    const favouritesArr = JSON.parse(localStorage.getItem('favourites')) || [];

    return (
        <div className='favourites'>
            <h2 className='favourites__heading'>Please checkout your favourite goods</h2>
            <div className='favourites__container'>
                {favouritesArr.map(el => {
                    return <GoodsItem price={el.price} name={el.name} art={el.art} color={el.color} url={el.url}
                                      className={el.className} key={el.art} changeState={changeState}
                                      getLocalStorageItems={getLocalStorageItems} cartBtn={true}/>
                })}
            </div>
        </div>
    );
};

Favourites.propTypes = {
    changeState: PropTypes.func.isRequired,
    getLocalStorageItems: PropTypes.func.isRequired
};

export default Favourites;


