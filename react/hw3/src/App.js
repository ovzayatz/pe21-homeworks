import React, {useEffect, useState} from 'react';
import './App.scss';
import AppRoutes from "./routes/AppRoutes";
import {NavLink} from "react-router-dom";

const App = () => {

    const [goods, setGoods] = useState([]);
    const [favourites, setFavourites] = useState(JSON.parse(localStorage.getItem('favourites')) || []);
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);

    useEffect(() => {
        getGoods()
    }, []);

    const getGoods = () => {
        fetch('./goodsList.json').then(r => r.json())
            .then(data => {
                setGoods(data)
            });
    };

    const getLocalStorageItems = (key) => {
        return JSON.parse(localStorage.getItem(key))
    };

    const changeState = () => {
        setFavourites(getLocalStorageItems('favourites') || []);
        setCart(getLocalStorageItems('cart') || []);
    };

    console.log('cartArr - - - ',cart );
    return (

        <div className="App">
            <div className='header'>
                <a href='/' className='logo'><span className='logo__text-p1'>hiking</span> <span
                    className='logo__text-p2'>shop</span> </a>
                <div>
                    <NavLink to={'/favourites'} className='header__link'>Favourites</NavLink>
                    <NavLink to={'/cart'} className='header__link'>Cart</NavLink>
                </div>
            </div>
            <AppRoutes goods={goods} favourites={favourites} cartArr={cart}
                       changeState={() => changeState()}
                       getLocalStorageItems={() => getLocalStorageItems()}
            />
        </div>
    );
};

export default App;

