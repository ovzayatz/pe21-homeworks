import React from 'react';
import {Route, Switch} from 'react-router-dom'
import Favourites from '../pages/Favourites/Favourites';
import Main from "../pages/Main/Main";
import Cart from "../pages/Cart/Cart";

const AppRoutes = ({favourites, cartArr, changeState, getLocalStorageItems, setCart}) => {

    return (
        <Switch>
            <Route exact path='/' render={() => <Main changeState={changeState}/>}/>
            <Route exact path='/favourites' render={() => <Favourites favourites={favourites} changeState={changeState}
                                                                      getLocalStorageItems={getLocalStorageItems}/>}/>
            <Route exact path='/cart' render={() => <Cart cartArr={cartArr}
                                                          changeState={changeState} setCart={setCart}/>}/>
        </Switch>
    );
};

export default AppRoutes;

