import React from "react";
import Button from "./Button";
import {fireEvent, render} from "@testing-library/react";
import '@testing-library/jest-dom/extend-expect';


describe('Button component', () => {

    test('should be rendered', () => {
        render(<Button onClick={() => {}} text=''/>)
    });

    test('should have text from props', () => {
        const buttonText = 'button-test';
        render(<Button onClick={() => {}} text={buttonText}/>);
        const buttonElement = document.getElementsByTagName('button')[0];
        expect(buttonElement.textContent).toBe(buttonText)
    });

    test('should have default class-name', () => {
        const container = render(<Button onClick={() => {}} text=''/>);
        expect(container.getByTestId('button')).toHaveClass('btn')
    });

    test('should have  background color from its props', () => {
        render(<Button onClick={() => {}} text='' backgroundColor='red'/>);
        const buttonElement = document.getElementsByTagName('button')[0];
        expect(buttonElement).toHaveStyle('background-color:red')
    });

    test ('onClick callback must be called after click', ()=>{
        const fn = jest.fn();
        const {getByTestId}=render(<Button onClick={fn} text='' />);
        fireEvent.click(
            getByTestId('button'),
            new MouseEvent('click', {bubbles:true})
        );
        expect(fn).toBeCalledTimes(1);
    })
});

