import React from "react";
import Modal from "./Modal";
import {shallow} from 'enzyme';
import {fireEvent} from "@testing-library/dom";
import {render} from "@testing-library/react";

describe('Modal component', () => {

    const fn = jest.fn();
    const props = {
        text: 'test modal text',
        name: 'test',
        onClick: fn,
        onSubmClick: () => {
        },
        url: 'www.test.com',
        header: 'test modal header',
        modalClassName: 'test-modal'
    };


    test('should be rendered according to props fulfilled', () => {

        const container = shallow(<Modal modalClassName={props.modalClassName} text={props.text} name={props.name}
                                         onClick={props.onClick} url={props.url} header={props.header}
                                         onSubmClick={props.onSubmClick}/>);

        expect(container.hasClass('test-modal')).toBeTruthy();
        expect(container.find('.modal__header-text').text()).toBe(props.header);
        expect(container.find('img').props().src).toBe(props.url);
        expect(container.find('.modal__text').text()).toBe(props.text);
    });

    test('onClick function from props delivered and works', () => {

        const {getByTestId} = render(<Modal modalClassName={props.modalClassName} text={props.text} name={props.name}
                                            onClick={props.onClick} url={props.url} header={props.header}
                                            onSubmClick={props.onSubmClick}/>);

        fireEvent.click(
            getByTestId('modal'),
            new MouseEvent('click', {bubbles: true})
        );

        expect(fn).toBeCalledTimes(1);
    });
});