import initialStore from "../initialStore";
import {FORM_DATA_PROCESSED} from "./cartFormActions";

export default function cartFormReducer(formDataFromStore = initialStore.formValues, {type, formData}) {

    switch (type) {
        case (FORM_DATA_PROCESSED): return formData;

        default: return formDataFromStore
    }

}