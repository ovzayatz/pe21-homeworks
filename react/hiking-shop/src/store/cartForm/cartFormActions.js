export const FORM_DATA_PROCESSED = 'FORM_DATA_PROCESSED';

export function loadingFormDataAction (valuesObject) {
return (dispatch) => {
    console.log('purchased goods ---', JSON.parse(localStorage.getItem('cart')));
    console.log('buyers data ---', valuesObject);
    localStorage.setItem('cart', '[]');

    dispatch({
        type: FORM_DATA_PROCESSED,
        formData: valuesObject
    })
}
}