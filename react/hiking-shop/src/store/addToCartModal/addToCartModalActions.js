export const TOGGLE_ADD_TO_CART_MODAL = 'TOGGLE_ADD_TO_CART_MODAL';

export const toggleCartModalAction = (id) => (dispatch, getStore) => {

    const isCartModalOpenStore = getStore().isCartModalOpen;

        dispatch({
            type: TOGGLE_ADD_TO_CART_MODAL,
            payload: !isCartModalOpenStore ? id : 0
        })
};

