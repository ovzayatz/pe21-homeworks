import {TOGGLE_ADD_TO_CART_MODAL, toggleCartModalAction} from "./addToCartModalActions";
import configureStore from 'redux-mock-store'
import thunk from "redux-thunk";

const middlewares = [thunk];
const mockStore = configureStore(middlewares);


describe('toggleCartModalAction should', () => {

    test('return action with id in payload if isCartModalOpen in store is 0', () => {
        const store = mockStore({isCartModalOpen: 0});
        store.dispatch(toggleCartModalAction(5));

        const expectedActions = [{
            type: TOGGLE_ADD_TO_CART_MODAL,
            payload: 5
        }];

        expect(store.getActions()).toEqual(expectedActions)
    });

    test ('return action with "0" in payload if isCartModalOpen in store is id of product', ()=>{
        const store = mockStore({isCartModalOpen: 5});
        store.dispatch(toggleCartModalAction(5));

        const expectedActions = [{
            type: TOGGLE_ADD_TO_CART_MODAL,
            payload: 0
        }];

        expect (store.getActions()).toEqual(expectedActions)
    })
});