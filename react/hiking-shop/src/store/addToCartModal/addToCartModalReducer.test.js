import {TOGGLE_ADD_TO_CART_MODAL} from "./addToCartModalActions";
import addToCartModalReducer from "./addToCartModalReducer";

describe ('add to cart modal reducer should', ()=>{
   test ('return new store "isCartModalOpenStore" property value', ()=>{
       expect (addToCartModalReducer(0, {
           type: TOGGLE_ADD_TO_CART_MODAL,
           payload: 1
       })).toBe(1)
   });
   test('return default value in case action payload is not provided', ()=>{
       expect (addToCartModalReducer(5, {type:TOGGLE_ADD_TO_CART_MODAL})).toBe(5)
   });

    test('return default value in case action type is not provided', ()=>{
        expect (addToCartModalReducer(0, {type:'', payload: 5})).toBe(0)
    })
});