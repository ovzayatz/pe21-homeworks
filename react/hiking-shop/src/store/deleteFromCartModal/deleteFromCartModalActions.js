export const TOGGLE_DELETE_FROM_CART_MODAL = 'TOGGLE_DELETE_FROM_CART_MODAL';

export const toggleDelFromCartModalAction = (id) => (dispatch, getStore) => {

    const isDelCartModalOpenStore = getStore().isDelCartModalOpen;

    dispatch({
        type: TOGGLE_DELETE_FROM_CART_MODAL,
        payload: !isDelCartModalOpenStore ? id : 0

    })
};