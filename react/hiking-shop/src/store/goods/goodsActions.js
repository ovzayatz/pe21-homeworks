export const GOODS_LOADED = 'GOODS_LOADED';

export function loadingGoodsAction() {
    return (dispatch) => {
        // fetch('./goodsList.json').then(r => r.json())
        fetch('/api/goodsList').then(r => r.json())

            .then(result => {
                dispatch({
                    type: GOODS_LOADED,
                    result: result
                })
            });

    }
}

