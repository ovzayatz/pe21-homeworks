import React from "react";
import GoodsItem from "../../components/GoodsItem/GoodsItem";
import PropTypes from 'prop-types';

const Cart = ({changeState}) => {

    const cartArr = JSON.parse(localStorage.getItem('cart')) || [];

    const cartSection = cartArr &&
        <div className='section'>
            <h2 className='section-heading'>Please checkout your cart</h2>
            <div className='goods-list'>
                {cartArr.map(el => {
                    return <GoodsItem price={el.price} name={el.name} art={el.art} color={el.color} url={el.url}
                                      className={el.className} key={el.art} cartBtn={false} delCartBtn={true}
                                      changeState={changeState}
                    />
                })}
            </div>
        </div>;

    if (cartArr.length > 0) {
        return (cartSection)
    } else {
        return <h2 className='section-heading'>Your cart is empty</h2>;
    }
};

Cart.propTypes = {
    cartArr: PropTypes.array.isRequired,
    changeState: PropTypes.func.isRequired
};


export default Cart;