import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store/configStore'

ReactDOM.render(
    <React.StrictMode>
        <ErrorBoundary>
            <Provider store={store}>
            <BrowserRouter>
                <App/>
            </BrowserRouter>
            </Provider>
        </ErrorBoundary>
    </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();

