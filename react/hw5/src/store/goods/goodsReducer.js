import initialStore from "../initialStore";
import {GOODS_LOADED} from "./goodsActions";

export default function goodsReducer(goodsFromStore = initialStore.goods, {type,result}) {
    switch (type) {
        case GOODS_LOADED: return [
            ...goodsFromStore,
            ...result];

        default: return goodsFromStore
    }
}