import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import './GoodsItem.scss'
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import {connect} from "react-redux";
import {toggleCartModalAction} from "../../store/addToCartModal/addToCartModalActions";
import {toggleDelFromCartModalAction} from "../../store/deleteFromCartModal/deleteFromCartModalActions";

const GoodsItem = ({
                       name,
                       price,
                       url,
                       art,
                       color,
                       className,
                       cartBtn,
                       delCartBtn,
                       changeState,
                       isCartModalOpen,
                       switchCartModalOpen,
                       isDelCartModalOpen,
                       switchDelCartModalOpen
                   }) => {
    const [isFavorite, switchFavourite] = useState(false);
    const addItemLocalStorage = (key, item) => {
        if (localStorage.getItem(key)) {
            let keyArr = JSON.parse(localStorage.getItem(key));
            if (!keyArr.includes(item)) {
                keyArr.push(item);
                localStorage.setItem(key, JSON.stringify(keyArr))
            }
        } else {
            localStorage.setItem(key, JSON.stringify([item]))
        }
    };

    const deleteItemLocalStorage = (key, id) => {
        let arr = JSON.parse(localStorage.getItem(key));
        const elToDelete = arr.find(e => e.art === id);
        arr.splice(arr.indexOf(elToDelete), 1);
        localStorage.setItem(key, JSON.stringify(arr))
    };

    useEffect(() => {
        const favouritesArr = JSON.parse(localStorage.getItem('favourites'));
        if (favouritesArr) {
            if (favouritesArr.find(e => e.art === art)) {
                switchFavourite(true);
            }

        }
    }, [art]);


    const cartModal = isCartModalOpen === art && <Modal
        price={price}
        color={color}
        className={className}
        name={name}
        header={`add ${name} to cart`}
        text={`Please push Ok to add ${name} to cart`}
        modalClassName='cart-modal'
        url={url}
        art={art}
        onClick={(event) => {
            if (event.target === event.currentTarget) switchCartModalOpen(0)
        }}
        onSubmClick={() => addToCart({name, url, art, color, price, className})}
        onCancelClick={() => switchCartModalOpen(0)}
    />;

    const delFromCartModal = isDelCartModalOpen === art && <Modal
        price={price}
        color={color}
        className={className}
        name={name}
        header={`Delete ${name} from cart`}
        text={`Please push Ok to delete ${name} from cart`}
        modalClassName='cart-modal'
        url={url}
        art={art}
        onClick={(event) => {
            if (event.target === event.currentTarget) switchDelCartModalOpen(0)
        }}
        onSubmClick={() =>
            deleteFromCart()
        }
        onCancelClick={() => switchDelCartModalOpen(0)}
    />;

    const addToCart = (item) => {
        if (localStorage.getItem('cart')) {
            let cart = JSON.parse(localStorage.getItem('cart'));
            cart.push(item);
            localStorage.setItem('cart', JSON.stringify(cart))
        } else {
            localStorage.setItem('cart', JSON.stringify([item]))
        }
        alert(`${name} is added to your cart`);
        switchCartModalOpen(!isCartModalOpen);
    };

    const deleteFromCart = () => {
        switchDelCartModalOpen(0);
        deleteItemLocalStorage('cart', art);
        changeState();
    };

    const switchFavouritesItem = () => {
        if (!isFavorite) {
            addItemLocalStorage('favourites', {name, price, url, art, color, className});
            switchFavourite(!isFavorite)
        } else {
            switchFavourite(!isFavorite);
            deleteItemLocalStorage('favourites', art);
            changeState()
        }
    };

    return (
        isCartModalOpen ? cartModal : isDelCartModalOpen ? delFromCartModal :
            <div className={className}>
                {delCartBtn ? <Button text='&times;' className='btn' onClick={() =>
                    switchDelCartModalOpen(art)}/> : undefined}
                <div><img alt='item ' src={url}/></div>
                <div className={'goods-item__name-wrapper'}>
                    <span>{name}</span>
                    <img className='goods-item__fav-icon'
                         src={!isFavorite ? './icons/star_grey.png' : './icons/star_gold.png'}
                         alt='favourites icon'
                         onClick={
                             () => {
                                 switchFavouritesItem()
                             }
                         }/>
                </div>
                <p>{`Price: ${price}`}</p>
                <p>{`ID: ${art}`}</p>
                <p>{`Color: ${color}`}</p>
                {cartBtn ? <Button className='goods-item_cart-button btn' text='Add to cart'
                                   onClick={() => switchCartModalOpen(art)}/> : undefined}
            </div>
    );
};


GoodsItem.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
    art: PropTypes.number.isRequired,
    color: PropTypes.string,
    className: PropTypes.string
};

GoodsItem.defaultProps = {
    color: 'in stock',
    className: 'goods-item'
};

const mapStoreToProps = (store) => {
    return {
        isCartModalOpen: store.isCartModalOpen,
        isDelCartModalOpen: store.isDelCartModalOpen
    }
};

const mapDispatchToProps = (dispatch) => ({
    switchCartModalOpen: (id) => dispatch(toggleCartModalAction(id)),
    switchDelCartModalOpen: (id) => dispatch(toggleDelFromCartModalAction(id))
});

export default connect(mapStoreToProps, mapDispatchToProps)(GoodsItem);

