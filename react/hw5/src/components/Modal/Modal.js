import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import './Modal.scss'


const Modal = ({ url, header, text, modalClassName, onClick, onSubmClick, onCancelClick}) => {

    return (
        <div className={modalClassName} onClick={onClick}>
            <div className='cart-modal__window'>
                <div className='modal__header'>
                    <p className='modal__header-text'>{header}</p>
                </div>
                <img alt='item' src={url}/>
                <p>{text}</p>
                <Button className='cart-modal__btn btn' text='Ok' onClick={() => {onSubmClick()}}/>
                <Button className='cart-modal__btn btn' text='Cancel' onClick={() => {onCancelClick()}}
                />

            </div>
        </div>
    );
};


Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    className: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    url: PropTypes.string.isRequired,
    art: PropTypes.number.isRequired,
    onSubmClick: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired
};

Modal.defaultProps = {
    className: 'modal'
};

export default Modal;