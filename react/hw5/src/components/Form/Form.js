import React from 'react';
import {Formik, Form, Field} from "formik";
import './initialValues';
import {initialValues} from "./initialValues";
import './Form.scss'
import {number, object, string} from "yup";
import {connect} from "react-redux";
import {loadingFormDataAction} from "../../store/cartForm/cartFormActions";

const CartForm = ({saveFormData, changeAppState}) => {

    const saveFormValues = (formValuesObj) => {
        saveFormData(formValuesObj);
        changeAppState()
    };

    return (
        <Formik
            validationSchema={
                object({
                    firstName: string().required().min(2, 'too short first name').max(30, 'too long first name'),
                    secondName: string().required().min(2, 'too short second name').max(30, 'too long second name'),
                    age: number().required().min(12, 'you are too young to buy this stuff').max(100, 'you are too old for this sh*t'),
                    address: string().required('you must enter your address for goods delivery'),
                    phone: number('phone number contains only numbers').required('you must enter your contact phone'),
                    email: string().required().email()
                })
            }
            initialValues={initialValues}
            onSubmit={(values) => saveFormValues(values)}
        >

            {({errors, touched}) => (
                <Form className='form'>
                    <h3>Please enter your data to buy goods from the cart</h3>
                    <label>
                        <Field name='firstName' className='form__field' placeholder='Enter your first name'/>
                        {touched.firstName && errors.firstName ?
                            <span className='error-message'> {errors.firstName}</span>
                            : null}
                    </label>

                    <label>
                        <Field name='secondName' className='form__field' placeholder='Enter your second name'/>
                        {touched.secondName && errors.secondName ?
                            <span className='error-message'> {errors.secondName}</span>
                            : null}
                    </label>

                    <label>
                        <Field name='age' type='number' className='form__field' placeholder='Enter your age'/>
                        {touched.age && errors.age ?
                            <span className='error-message'> {errors.age}</span>
                            : null}
                    </label>

                    <label>
                        <Field name='address' className='form__field' placeholder='Enter delivery address'/>
                        {touched.address && errors.address ?
                            <span className='error-message'> {errors.address}</span>
                            : null}
                    </label>

                    <label>
                        <Field name='phone' className='form__field' placeholder='Enter your mobile phone number'/>
                        {touched.phone && errors.phone ?
                            <span className='error-message'> {errors.phone}</span>
                            : null}
                    </label>

                    <label>
                        <Field name='email' className='form__field' placeholder='Enter your email'/>
                        {touched.email && errors.email ?
                            <span className='error-message'> {errors.email}</span>
                            : null}
                    </label>

                    <label className='checkout-lbl'>
                        <Field name='checkout'
                               type='submit' value='Checkout' className='btn checkout-btn'
                        />
                    </label>

                </Form>
            )}

        </Formik>

    );
};

const mapStoreToProps = (store) => {
    return {
        storeValues: store.formValues
    }
};

const mapDispatchToProps = (dispatch) => ({
    saveFormData: (valuesObject) => dispatch(loadingFormDataAction(valuesObject))
});

export default connect (mapStoreToProps, mapDispatchToProps) (CartForm);