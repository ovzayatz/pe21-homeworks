import React from 'react';
import GoodsItem from "../../components/GoodsItem/GoodsItem";
import {connect} from "react-redux";

const Main = ({goods, changeState}) => {

    return (

        <div className='goods-list'>
            {goods.map((el) => {
                return <GoodsItem className={'goods-item'} name={el.name} price={el.price} url={el.url} art={el.art}
                                  color={el.color} key={el.art} cartBtn={true} changeState={changeState}
                />
            })}
        </div>
    );
};

function mapStoreToProps (store) {
    return {goods: store.goods}
}

export default connect (mapStoreToProps)(Main);