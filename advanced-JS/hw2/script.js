const root = document.getElementById('root');
const booksList = document.createElement('ul');
root.append(booksList);

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

    books.forEach (el => {
        try {
            if (!el.author) {
                throw new Error(`Книга ${books.indexOf(el)+1} - Отсутствуют данные об авторе книги`);
            } else if (!el.name) {
                throw new Error(`Книга ${books.indexOf(el)+1} - Отсутствуют данные о названии книги`);
            } else if (!el.price) {
                throw new Error(`Книга ${books.indexOf(el)+1} - Отсутствуют данные о цене книги`);
            } else {
                const book = document.createElement('div');
                const author = document.createElement('p');
                const name = document.createElement('p');
                const price = document.createElement('p');
                author.innerText = el.author;
                name.innerText = el.name;
                price.innerText = `${el.price} usd`;
                book.classList.add('book-item');
                author.classList.add('book-author');
                name.classList.add('book-name');
                price.classList.add('book-price');
                book.append(author, name, price);
                booksList.append(book);
            }
        } catch (err) {
                console.log(`${err.name}: ${err.message}`)
            }

    });
