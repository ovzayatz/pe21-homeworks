'use strict';

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary
    }

    set name(value) {
        if (!isNaN(+value)) {
            console.error('please enter valid name');
            return
        }
        this._name = value
    }

    get name() {
        return this._name
    }


    get age() {
        return this._age
    }

    set age(value) {
        if (isNaN(+value) || +value <= 0) {
            console.error('please enter valid age');
            return
        }
        this._age = value
    }

    get salary() {
        return this._salary
    }

    set salary(value) {
        if (isNaN(+value) || +value < 0) {
            console.error('please enter valid salary');
            return
        }
        this._salary = value
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang = []) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3
    }

    set salary(value) {
        if (isNaN(+value) || +value < 0) {
            console.error('please enter valid salary');
            return
        }
        this._salary = value * 3
    }

    get lang() {
        return this._lang.join(', ')
    }

    set lang(value) {
        if (!isNaN(+value)) {
            console.error('please enter valid languages');
            return
        }
        this._lang = value
    }
}

const proger1 = new Programmer('johh', 25, 2000, ['js']);
const proger2 = new Programmer('jim', 53, 3000, ['java', 'python']);
const proger3 = new Programmer('jack', 25, 500, ['php']);

console.log(proger1, proger2, proger3);

const employee = new Employee('Dan', 15, 1000);
console.log(employee);


