function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

class WhackAMole {

    constructor() {
        this.cellsArr = [...document.getElementsByTagName('td')];

    }

    setGameTimeout() {
        if (this.level === 'hard') {
            this.timeout = 500
        } else if (this.level === 'medium') {
            this.timeout = 1000
        } else if (this.level === 'light') {
            this.timeout = 1500
        }
    }

    gamePlay() {

        const cellPainting = setInterval(() => {
            const activeCellsArr = [...document.getElementsByClassName('active')];
            // activeCellsArr[0].classList.replace('active', 'machine-score');
            activeCellsArr.forEach(e => e.classList.replace('active', 'machine-score'));
            const clearCellsArray = document.querySelectorAll('.clear');
            clearCellsArray[getRandomInt(clearCellsArray.length)].classList.replace('clear', 'active');
            // const activeCell = this.activeCells[0];
            const activeCell = document.querySelector('.active');
            activeCell.addEventListener('click', () => {
                activeCell.classList.replace('active', 'player-score')
            });
            if (clearCellsArray.length === this.cellsArr.length / 2) {
                activeCell.classList.remove('active');
                clearInterval(cellPainting)
            }
        }, this.timeout);

    }

    showScore() {
        const scoreUpdating = setInterval(() => {
                const playerScore = document.querySelectorAll('.player-score').length;
                const machineScore = document.querySelectorAll('.machine-score').length;
                const playerScoreElement = document.querySelector('.game__player-score');
                const machineScoreElement = document.querySelector('.game__machine-score');

                playerScoreElement.innerText = `${playerScore}`;
                machineScoreElement.innerText = `${machineScore}`;
                if (playerScore + machineScore === this.cellsArr.length / 2) {

                    clearInterval(scoreUpdating);
                    if (playerScore > machineScore) {
                        this.winner = 'Player'
                    } else if (machineScore > playerScore) {
                        this.winner = 'Machine'
                    } else {
                        this.winner = 'No one'
                    }
                    this.showWinner()
                }
            }, 0
        );
    }

    showWinner() {
        const winnerMsgBlock = document.createElement('div');
        const winnerMsg = document.createElement('p');
        winnerMsgBlock.classList.add('game__winner-block');
        const winnerMsgBlockBtn = document.createElement('button');
        winnerMsg.innerText = `${this.winner} wins!`;
        winnerMsgBlockBtn.innerText = 'OK';
        winnerMsg.classList.add("game__winner-block-text");
        winnerMsgBlockBtn.classList.add('game__winner-block-button');
        winnerMsgBlock.append(winnerMsg, winnerMsgBlockBtn);
        document.body.prepend(winnerMsgBlock);
        winnerMsgBlockBtn.addEventListener('click', () => {
            this.clearGame();
            winnerMsgBlock.remove();
        })
    }

    startGame() {

        const levelButtons = document.querySelectorAll('.game__menu-button');
        levelButtons.forEach((e) => {
            e.addEventListener('click', () => {
                    document.querySelector('.game__menu').style.display = 'none';
                    document.querySelector('.game__score-block').style.display = "flex";
                    this.level = e.dataset.level;
                    this.setGameTimeout();
                    this.gamePlay();
                    this.showScore();

                }
            );
        })
    }

    clearGame() {
        document.querySelectorAll('td').forEach(e => {
            e.className = 'game__play-area-cell clear';
            document.querySelector('.game__score-block').style.display = "none";
            document.querySelector('.game__menu').style.display = 'flex';
        });
    }
}

new WhackAMole().startGame();

