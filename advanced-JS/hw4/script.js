const starWarsBlock = document.getElementById('sw-block');

fetch('https://swapi.dev/api/films')
    .then(r => r.json())
    .then(data => {
        data.results.forEach((e) => {
            const episodeBlock = document.createElement('div');
            episodeBlock.classList.add('episode-container');
            const episodeNumber = document.createElement('p');
            episodeNumber.classList.add('episode-number');
            const title = document.createElement('p');
            title.classList.add('episode-title');
            const introduction = document.createElement('p');
            introduction.classList.add('episode-intro');
            const characters = document.createElement('p');
            characters.classList.add('episode-characters');
            title.innerText = e.title;
            episodeNumber.innerText = `EPISODE ${e.episode_id}`;
            introduction.innerText = e.opening_crawl;
            let charactersLinksArray = e.characters;
            let requests = charactersLinksArray.map(link => fetch(link));
            Promise.all(requests)
                .then(responses => Promise.all(responses.map(r => r.json())))
                .then(users => users.forEach(user => {
                    characters.innerHTML += `<span>${user.name}</span>`
                }));
            episodeBlock.append(episodeNumber, title, characters, introduction);
            starWarsBlock.append(episodeBlock);
            document.body.prepend(starWarsBlock)
        })
    });

