class Address {

    constructor(selector) {
        this.parent = document.querySelector(selector);
        this.addressElements = {
            continent: document.createElement('p'),
            region: document.createElement('p'),
            city: document.createElement('p'),
            district: document.createElement('p'),
            country: document.createElement('p')
        }
    }

    async getIPAddress() {
        await fetch('https://api.ipify.org/?format=json')
            .then(responce => responce.json())
            .then(data => {
                this.IPAddress = data.ip;
            });
    }

    async getAddressData() {
        await fetch(`http://ip-api.com/json/${this.IPAddress}?fields=status,message,continent,country,regionName,city,district`)
            .then(r => r.json())
            .then(d => {
                    this.continent = d.continent;
                    this.country = d.country;
                    this.region = d.regionName;
                    this.city = d.city;
                    this.district = d.district;
                }
            );
    }

    async renderPage() {
        const container = this.parent;
        const button = document.createElement('button');
        button.innerText = 'Вычислить по IP';
        container.append(button);
        await this.getIPAddress();
        await this.getAddressData();
        this.addressElements.continent.innerText = `continent - ${this.continent}`;
        this.addressElements.country.innerText = `country - ${this.country}`;
        this.addressElements.region.innerText = `region - ${this.region}`;
        this.addressElements.city.innerText = `city - ${this.city}`;
        if (this.district) {
        this.addressElements.district.innerText = `district - ${this.district}`;
        } else {
            this.addressElements.district.innerText = `district - no data`;
        }
        button.addEventListener('click', () => container.append(this.addressElements.continent, this.addressElements.country, this.addressElements.region, this.addressElements.city, this.addressElements.district)
        )
    }
}

function showAddressOnClick() {
    const newAddress = new Address('#container');
    newAddress.renderPage();
}

showAddressOnClick();



